import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "pokemons",
    component: () => import("@/views/Pokemons.vue"),
  },
  {
    path: "/pokemons",
    name: "pokemons",
    component: () => import("@/views/Pokemons.vue"),
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

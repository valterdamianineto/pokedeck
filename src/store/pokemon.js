import axios from "../axios.config";

const state = {
  pokemonInfo: [],
  evolutionChain: [],
  pokemonSpecies: {},
};

const actions = {
  async loadPokemonInfo({ commit }, pokemonId) {
    try {
      const { data } = await axios.get(`/pokemon/${pokemonId}`);
      commit("setPokemonInfo", data);
    } catch (error) {
      throw new Error(error);
    }
  },

  async loadPokemonSpecies({ commit }, pokemonName) {
    try {
      const { data } = await axios.get(`/pokemon-species/${pokemonName}`);
      commit("setPokemonSpecies", data);
    } catch (error) {
      throw new Error(error);
    }
  },

  async loadEvolutionChain({ commit }, pokemonId) {
    try {
      const { data } = await axios.get(`/evolution-chain/${pokemonId}`);
      commit("setPokemonEvolutionChain", data);
    } catch (error) {
      throw new Error(error);
    }
  },
};

const mutations = {
  setPokemonInfo: (state, pokemon) => {
    state.pokemonInfo = pokemon;
  },
  setPokemonEvolutionChain: (state, evolution) => {
    state.evolutionChain = evolution;
  },
  setPokemonSpecies: (state, species) => {
    state.pokemonSpecies = species;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};

import Vue from "vue";
import Vuex from "vuex";
import pokeList from "./pokeLIst";
import pokemon from "./pokemon";

Vue.use(Vuex);

const index = {
  namespaced: true,
  state: () => ({}),
  actions: {
    setErrorMessage({ commit }, { error }) {
      commit("SET_ERROR_MESSAGE", { error });
    }
  },
  mutations: {
    ["SET_ERROR_MESSAGE"](state, { error }) {
      state.errorMessage = error;
    }
  }
};

export default new Vuex.Store({
  modules: {
    index,
    pokeList,
    pokemon
  }
});

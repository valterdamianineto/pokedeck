import axios from "axios";

const baseUrl = "https://pokeapi.co/api/v2";

const axiosConfig = axios.create({ baseURL: baseUrl });
axiosConfig.interceptors.request.use(
  function(config) {
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

axiosConfig.interceptors.response.use(
  (response) => response,
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosConfig;

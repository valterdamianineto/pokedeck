import axios from "../axios.config";

const state = {
  pokemonList: [],
  pokemonDetailList: [],
};

const actions = {
  async loadPokemonList({ commit }) {
    try {
      const { data } = await axios.get("/pokemon/?limit=250");
      commit("setPokemonList", data);
    } catch (error) {
      throw new Error(error);
    }
  },
};

const mutations = {
  setPokemonList: (state, pokemons) => {
    state.pokemonList = pokemons;
  },

  setDetailList: (state, list) => {
    state.pokemonDetailList = list;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};

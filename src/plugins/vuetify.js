import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        primary: "#3267b1",
        secondary: "#ffcb08",
        red: "#ed3237",
        grey: "#bcbec0",
      },
    },
  },
});

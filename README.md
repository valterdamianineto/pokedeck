# Pokedeck

<p align="center" fontSize="60px">
  Platform for listing pokemons
</p>


## Project

The PokeDeck pokemon listing platform, was built using the Vue framework, based on JavaScript, the system generates a list of pokemons that can be sorted based on name or type, as well as displaying a search field that lets you search for the pokemon by name or type.

The home screen displays a summary with some data about the clicked pokemon, and presents a button with the option of more details about this pokemon.

The data displayed in this application was consumed from the REST API [PokéApi](https://pokeapi.co/) using Axios and handled internally with the Vuex state manager

### Functionalities
- [x] **Pokemons List**: List the Pokemons using the REST API.

- [x] **Search pokemons**: Method to filter the pokemons by their name or type.

- [x] **Table Pagination**: Method to add more Pokemons to the list shown to the user.

- [x] **Display details about pokemon**: Create a page in the application with more details about the chosen pokemon.

- [x] **Evolution Tree**: Build the pokemon's evolution tree.

- [x] **Summary Section**: Basic information about the Pokémon, such as image, experience and abilities.

##  Technologies

-  [JavaScript](https://www.javascript.com/)
-  [Vue](https://vuex.vuejs.org/)
-  [Vuex](https://vuex.vuejs.org/)
-  [Axios](https://axios-http.com/)
-  [Vue Router](https://router.vuejs.org/)
-  [Vuetify](https://vuetifyjs.com/en/)


### Notes

- The list of pokemons was limited to 250 pokemons when calling the request in the pokeList file.

## 📥 Installation and execution

Clone this repository and access the directory.

```bash
$ git clone git@gitlab.com:valterdamianineto/pokedeck.git && cd pokedeck
```

```bash
# To run this project it is necessary to have Node installed, as well as Yarn or NPM

# Installing dependencies
$ yarn

# Running application
$ yarn serve

# The system default execution port is:
$ http://localhost:8080/

```
